package structure;

public class Edge {
    private int from_id,end_id;
    private Vertex from_v,end_v;

    public Edge() {
        from_v=new Vertex();
        end_v=new Vertex();
            }

    public int getFrom_id() {
        return from_id;
    }

    public void setFrom_id(int from_id) {
        this.from_id = from_id;
    }

    public int getEnd_id() {
        return end_id;
    }

    public void setEnd_id(int end_id) {
        this.end_id = end_id;
    }

    public Vertex getFrom_v() {
        return from_v;
    }

    public void setFrom_v(Vertex from_v) {
        this.from_v = from_v;
    }

    public Vertex getEnd_v() {
        return end_v;
    }

    public void setEnd_v(Vertex end_v) {
        this.end_v = end_v;
    }
}
