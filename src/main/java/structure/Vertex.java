package structure;

import java.util.HashSet;
import java.util.Set;

public class Vertex {
    private int id;
    private Set<Integer> neighbors_id;
    private Set<Vertex> neighbors;

    public Vertex(int id, Set<Integer> neighbors_id, Set<Vertex> neighbors) {
        this.id = id;
        neighbors=new HashSet<>();
        neighbors_id=new HashSet<>();
        this.neighbors_id = neighbors_id;
        this.neighbors = neighbors;
    }

    public Vertex() {
        neighbors=new HashSet<>();
        neighbors_id=new HashSet<>();
    }

    public Vertex(int id) {
        neighbors=new HashSet<>();
        neighbors_id=new HashSet<>();
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Integer> getNeighbors_id() {
        return neighbors_id;
    }

    public void setNeighbors_id(Set<Integer> neighbors_id) {
        this.neighbors_id = neighbors_id;
    }

    public Set<Vertex> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(Set<Vertex> neighbors) {
        this.neighbors = neighbors;
    }

    public void addNeighbor(String neig)
    {
        Integer n=Integer.parseInt(neig);
        this.neighbors_id.add(n);
    }
}
