import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import scala.Array;
import scala.Tuple2;
import structure.Edge;
import structure.Vertex;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

public class main {


    public static void main(String args[]) {

        //   SparkConf conf=new SparkConf().setMaster("local").setAppName("scan");
        //  JavaSparkContext sc = new JavaSparkContext(conf);
        JavaSparkContext sc = new JavaSparkContext("local[2]", "Spark SCAN");
        JavaRDD<String> lines = sc.textFile("G");

        JavaPairRDD<String, String> edges = lines.flatMapToPair(new PairFlatMapFunction<String, String, String>() {
            @Override
            public Iterable<Tuple2<String, String>> call(String s) throws Exception {
                String parts[] = s.split("\t");
                Vector<Tuple2<String, String>> edge = new Vector<Tuple2<String, String>>();

                edge.add(new Tuple2<>(parts[0], parts[1]));
                edge.add(new Tuple2<>(parts[1], parts[0]));
                edge.add(new Tuple2<>(parts[0], parts[0]));
                return edge;
            }
        });
        JavaPairRDD<String, Iterable<String>> links = edges.distinct().groupByKey();
        JavaRDD<Vertex> vertices = links.map(new Function<Tuple2<String, Iterable<String>>, Vertex>() {
            @Override
            public Vertex call(Tuple2<String, Iterable<String>> stringIterableTuple2) throws Exception {
                Vertex v = new Vertex();
                v.setId(new Integer(stringIterableTuple2._1));
                Iterator it = stringIterableTuple2._2.iterator();
                while ((it.hasNext())) {
                    v.addNeighbor(it.next().toString());
                }
                return v;
            }
        });

        JavaPairRDD<Vertex, Vertex> couples = vertices.cartesian(vertices);
        JavaPairRDD<Vertex, Vertex> couples2 = couples.filter(new Function<Tuple2<Vertex, Vertex>, Boolean>() {
            @Override
            public Boolean call(Tuple2<Vertex, Vertex> vertexVertexTuple2) throws Exception {
                if (vertexVertexTuple2._1.getNeighbors_id().contains(vertexVertexTuple2._2.getId()) && vertexVertexTuple2._1.getId() != vertexVertexTuple2._2.getId())

                    return true;
                else
                    return false;
            }
        });

        couples2.foreach(new VoidFunction<Tuple2<Vertex, Vertex>>() {
            @Override
            public void call(Tuple2<Vertex, Vertex> vertexVertexTuple2) throws Exception {
                System.out.println(vertexVertexTuple2._1.getId() + "->" + vertexVertexTuple2._2.getId());
            }
        });
        
    }
}
